package com.pandemiastudio.redo.taskactivities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.pandemiastudio.redo.AddTask
import com.pandemiastudio.redo.R
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pandemiastudio.redo.EditTask
import com.pandemiastudio.redo.database.TasksRepository
import com.pandemiastudio.redo.database.repository.ListRepository
import com.pandemiastudio.redo.database.schema.TasksSchema
import com.pandemiastudio.redo.listactivities.ListList
import com.pandemiastudio.redo.recycleradapters.TaskItemAdapter


class TaskListActivity : AppCompatActivity() {

    private var taskList = ArrayList<TasksSchema>()
    private lateinit var adapter : TaskItemAdapter

    private lateinit var listId : String
    private lateinit var listType : String
    private lateinit var householdId : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_list)

        listId = intent.getStringExtra("listId").toString()
        listType = intent.getStringExtra("listType").toString()
        householdId = intent.getStringExtra("householdId").toString()


        adapter = TaskItemAdapter(this, taskList, listType, listId, householdId)

        val recyclerView : RecyclerView = findViewById(R.id.taskListRecycler)
        readTasksFromDatabase()

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

    fun addItemClicked(view: View) {
        val intent = Intent(applicationContext, AddTask::class.java)
        intent.putExtra("listId", listId)
        intent.putExtra("listType", listType)
        intent.putExtra("householdId", householdId)
        startActivity(intent)
    }

    fun editItem(view: View) {
        val intent = Intent(applicationContext, EditTask::class.java)
        intent.putExtra("listId", listId)
        intent.putExtra("listType", listType)
        intent.putExtra("householdId", householdId)
        startActivity(intent)
    }

    private fun readTasksFromDatabase() {
        TasksRepository().addOnGetTasksListener(listId, taskList) {
            taskList.add(it)
            adapter.notifyDataSetChanged()
            if(taskList != null && checkIfListCompleted()) {
                ListRepository().updateListCompleted(householdId, listId, true) {}
            } else if(taskList != null && !checkIfListCompleted()) {
                ListRepository().updateListCompleted(householdId, listId, false) {}
            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()

        val bIntent = Intent(this, ListList::class.java)
        bIntent.putExtra("householdId", householdId)
        startActivity(bIntent)
    }

    private fun checkIfListCompleted() : Boolean {
        var flag = true
        taskList.forEach {
            if(!it.completed) {
                flag = false
            }
            //Log.d("TUTAJPACZ", it.toString())
        }
        return flag
    }

}