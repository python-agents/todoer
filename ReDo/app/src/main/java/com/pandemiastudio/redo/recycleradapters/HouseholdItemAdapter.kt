package com.pandemiastudio.redo.recycleradapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pandemiastudio.redo.HouseholdMenu
import com.pandemiastudio.redo.HouseholdType
import com.pandemiastudio.redo.R
import com.pandemiastudio.redo.database.schema.HouseholdSchema

class HouseholdItemAdapter(private var context: Context, private var householdList: ArrayList<HouseholdSchema>):
        RecyclerView.Adapter<HouseholdItemAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var householdImage : ImageView = view.findViewById(R.id.householdItemImageView)
        val titleText : TextView = view.findViewById(R.id.householdItemNameTextView)
        val categoryText : TextView = view.findViewById(R.id.householdItemCategoryTextView)
        val backgroundLayout : LinearLayout = view.findViewById(R.id.householdItemBackgroundLayout)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
                R.layout.household_item,
                parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = householdList[position]



        holder.titleText.text = item.name
        holder.categoryText.text = item.type

        val image  = when(item.type) {
            HouseholdType.FAMILY.value -> R.drawable.ic_family
            HouseholdType.DORMITORY.value -> R.drawable.ic_graduated
            HouseholdType.ROOMMATES.value -> R.drawable.ic_apartment
            else -> R.drawable.ic_household_icon
        }

        val color = when(item.type) {
            HouseholdType.FAMILY.value -> context.getColor(R.color.cherry)
            HouseholdType.DORMITORY.value -> context.getColor(R.color.crane_blue)
            HouseholdType.ROOMMATES.value -> context.getColor(R.color.crane_green)
            else -> context.getColor(R.color.crane_orange_500)

            //context.getColor(R.color.completed)
        }

        holder.householdImage.setImageResource(image)
        holder.backgroundLayout.setBackgroundColor(color)


        holder.backgroundLayout.setOnClickListener() {
            val intent = Intent(context, HouseholdMenu::class.java)
            intent.putExtra("householdId", item.id)
            context.startActivity(intent)
        }


    }

    override fun getItemCount() = householdList.size


}