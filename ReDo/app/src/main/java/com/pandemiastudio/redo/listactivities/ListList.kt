package com.pandemiastudio.redo.listactivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pandemiastudio.redo.HouseholdMenu
import com.pandemiastudio.redo.R
import com.pandemiastudio.redo.database.repository.HouseholdRepository
import com.pandemiastudio.redo.database.schema.ListSchema
import com.pandemiastudio.redo.recycleradapters.ListItemAdapter

class ListList : AppCompatActivity() {


    private var listList = ArrayList<ListSchema>()
    private lateinit var adapter : ListItemAdapter
    private lateinit var householdId : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_list)

        householdId = intent.getStringExtra("householdId").toString()

        adapter = ListItemAdapter(this, listList, householdId)


        readListsFromDatabase()

        val recyclerView : RecyclerView = findViewById(R.id.listListRecycler)


        recyclerView.adapter = adapter

        recyclerView.layoutManager = LinearLayoutManager(this)

    }

    private fun readListsFromDatabase() {

        HouseholdRepository().getHousehold(householdId) { it ->
            listList.clear()
            it.lists.forEach {
                listList.add(it)
                adapter.notifyDataSetChanged()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        val bIntent = Intent(this, HouseholdMenu::class.java)
        bIntent.putExtra("householdId", householdId)
        startActivity(bIntent)
    }
}
