package com.pandemiastudio.redo.database.schema

data class TasksSchema (
    var id: String = "",
    var taskName: String = "",
    var description: String = "",
    var completed: Boolean = false,
    var cost: Double = 0.0
)