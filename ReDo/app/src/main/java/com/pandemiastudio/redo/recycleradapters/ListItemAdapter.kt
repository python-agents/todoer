package com.pandemiastudio.redo.recycleradapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pandemiastudio.redo.EditTask
import com.pandemiastudio.redo.ListTypes
import com.pandemiastudio.redo.R
import com.pandemiastudio.redo.database.repository.ListRepository
import com.pandemiastudio.redo.database.schema.ListSchema
import com.pandemiastudio.redo.listactivities.EditListActivity

import com.pandemiastudio.redo.taskactivities.TaskListActivity
import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

class ListItemAdapter(private var context: Context, private var todoList: ArrayList<ListSchema>, private var householdId : String):
        RecyclerView.Adapter<ListItemAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val titleText : TextView = view.findViewById(R.id.list_item_title)
        val descriptionText : TextView = view.findViewById(R.id.list_item_description)
        val dateText : TextView = view.findViewById(R.id.list_item_date)
        val backgroundLayout : LinearLayout = view.findViewById(R.id.list_item_background)
        val costText : TextView = view.findViewById(R.id.list_item_cost)
        val priorityText : TextView = view.findViewById(R.id.list_item_important)

        val completedIcon : ImageView = view.findViewById(R.id.list_item_completed_icon)
        val deleteButton : ImageView = view.findViewById(R.id.list_item_delete_button)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
                R.layout.list_item,
                parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = todoList[position]
        val due_date = Instant.fromEpochMilliseconds(item.dueDate).toLocalDateTime(TimeZone.currentSystemDefault())

        holder.titleText.text = item.listName
        holder.descriptionText.text = item.description
        holder.dateText.text = "Due to: ${due_date.dayOfMonth}/${due_date.monthNumber}/${due_date.year}"
        holder.costText.text = "Paid: " + item.cost.toString()

        if(item.type == ListTypes.FINANCIAL.value) {
            holder.costText.visibility = TextView.VISIBLE
            holder.backgroundLayout.setBackgroundColor(context.getColor(R.color.crane_green))
        }

        if(item.completed) {
            setListCompleted(holder)
        }

        holder.priorityText.text = when (item.priority) {
            1L -> "Not important"
            2L -> "Important"
            else -> "Very important"
        }



        holder.backgroundLayout.setOnClickListener {
            val intent = Intent(context, TaskListActivity::class.java)
            intent.putExtra("listId", item.id)
            intent.putExtra("listType", item.type)
            intent.putExtra("householdId", householdId)
            context.startActivity(intent)
        }

        holder.backgroundLayout.setOnLongClickListener{
            val intent1 = Intent(context, EditListActivity::class.java)
            intent1.putExtra("householdId", householdId)
            intent1.putExtra("listId", item.id)
            intent1.putExtra("listName", item.listName)
            intent1.putExtra("priority", item.priority)
            intent1.putExtra("dueDate", item.dueDate)
            intent1.putExtra("description", item.description)
            context.startActivity(intent1)

            return@setOnLongClickListener true
        }

        //setListCompleted(holder)

        holder.deleteButton.setOnClickListener {
            val t_position: Int = todoList.indexOf(item)
            ListRepository().deleteList(householdId, item.id) {}
            todoList.removeAt(t_position)
            notifyItemRemoved(t_position)
            notifyItemRangeChanged(t_position, todoList.size)
        }

    }

    override fun getItemCount() = todoList.size

    private fun setListCompleted(holder: ViewHolder) {
        holder.completedIcon.visibility = ImageView.VISIBLE
        holder.deleteButton.visibility = ImageView.GONE
        holder.backgroundLayout.setBackgroundColor(context.getColor(R.color.completed))
    }



}