package com.pandemiastudio.redo

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton

class EditTask: AddTask() {
    private var taskID: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        taskID = intent.getStringExtra("id") as String
        title = intent.getStringExtra("task_name") as String
        val description = intent.getStringExtra("description") as String

        findViewById<EditText>(R.id.fillTitle).append(title)
        findViewById<EditText>(R.id.fillDesc).append(description)
        findViewById<Button>(R.id.buSubmit).text = "Submit changes"
    }

    override fun submit(view: View) {
        resultIntent.putExtra("taskID", taskID)
        super.submit(view) // hast to be invoked as last
    }
}