package com.pandemiastudio.redo.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import com.google.firebase.auth.FirebaseAuth
import com.pandemiastudio.redo.MainActivity
import com.pandemiastudio.redo.R
import com.pandemiastudio.redo.database.repository.UsersRepository
import kotlinx.android.synthetic.main.activity_register.*
import www.sanju.motiontoast.MotionToast

class RegisterActivity : AppCompatActivity() {
    private lateinit var  auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        title="Register"

        auth= FirebaseAuth.getInstance()
    }

    fun register(view: View){
        val email=editTextEmailAddress.text.toString()
        val password=editTextPassword.text.toString()
        val passwordConfirm = editTextPassword2.text.toString()

        if (password == passwordConfirm) {
            auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener { task ->
                if(task.isSuccessful){
                    UsersRepository().updateUserInfo {
                        Log.d("Login update", "Updated")
                    }
                    val intent= Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }.addOnFailureListener { exception ->
                Toast.makeText(applicationContext,exception.localizedMessage,Toast.LENGTH_LONG).show()
            }
        } else {
            MotionToast.createColorToast(this,
                    "Failed ☹️",
                    "Password and password confirm fields do not match",
                    MotionToast.TOAST_WARNING,
                    MotionToast.GRAVITY_BOTTOM,
                    MotionToast.LONG_DURATION,
                    ResourcesCompat.getFont(this, R.font.helvetica_regular))
        }


    }

    fun goToLogin(view: View){
        val intent= Intent(this,LoginActivity::class.java)
        startActivity(intent)
    }
}
