package com.pandemiastudio.redo

import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.pandemiastudio.redo.auth.EditProfileActivity
import com.pandemiastudio.redo.data.MenuItem
import com.pandemiastudio.redo.database.repository.UsersRepository
import com.pandemiastudio.redo.recycleradapters.MenuItemAdapter
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation


class MainMenu : AppCompatActivity() {
    private val menuItems = ArrayList<MenuItem>()
    private val adapter = MenuItemAdapter(this, menuItems)
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
        auth = FirebaseAuth.getInstance()

        val currentUser = auth.currentUser!!

        setAvatarImage(findViewById(R.id.mainMenuAvatarImageView))
        val usernameTextField: TextView = findViewById(R.id.mainMenuUsername)

        if (currentUser.displayName == "" || currentUser.displayName == "null" ||  currentUser.displayName == null) {
            usernameTextField.text = currentUser.email
        } else {
            usernameTextField.text = currentUser.displayName
        }

        val recyclerView: RecyclerView = findViewById(R.id.mainMenuRecycler)

        addItems()

        recyclerView.adapter = adapter

        var spanCount = when (resources.configuration.orientation) {
            Configuration.ORIENTATION_PORTRAIT -> 2
            else -> 3
        }

        recyclerView.layoutManager = GridLayoutManager(this, spanCount)
    }

    fun signOut(view: View) {
        auth.signOut()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun editProfile(view: View) {
        val intent = Intent(this, EditProfileActivity::class.java)
        startActivity(intent)
    }

    private fun setAvatarImage(imageView: ImageView) {
        val transformation: Transformation = RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .borderWidthDp(3f)
                .cornerRadiusDp(30f)
                .oval(false)
                .build()

        if (auth.currentUser!!.photoUrl != null) {
            Picasso.get()
                    .load(auth.currentUser!!.photoUrl)
                    .transform(transformation)
                    .resize(200, 200)
                    .into(imageView)
        } else {
            imageView.setImageResource(R.drawable.default_profile_img)
        }
    }

    private fun addItems() {
//        var newTaskId: String = ""
//        TasksRepository().addNewTask(
//            "c5e56812-50ca-42fa-a5c7-07a7a2d6efd7",
//            "Wynieś śmieci",
//            "Szybko",
//            0.0
//        ) {
//            Log.d("TAG_MAIN_MENU", "DONE $it")
//            newTaskId = it
//
//            TasksRepository().editTask(
//                    "c5e56812-50ca-42fa-a5c7-07a7a2d6efd7",
//                    newTaskId,
//                    mapOf(
//                        "taskName" to "Updated task name",
//                        "description" to "Updated task description",
//                        "priority" to 10.toLong(),
//                        "cost" to 1.100
//                        "description" to "Updated task description"
//                    ),
//                ) {
//                    Log.d("ITEM", "Task edit done")
//                    TasksRepository().deleteTask(
//                        "c5e56812-50ca-42fa-a5c7-07a7a2d6efd7",
//                        newTaskId,
//                    ) {
//                        Log.d("ITEM", "Task completed")
//                    }
//                }
//        }

//        var newTaskId: String = ""
//        ListRepository().addNewTaskList(
//            "c5e56812-50ca-42fa-a5c7-07a7a2d6efd7",
//            "HWDP",
//            "SOME",
//            "DESCRIPTION",
//            2,
//            2,
//        ) {
//            Log.d("TAG_MAIN_MENU", "DONE $it")
//            newTaskId = it
//
//            TasksRepository().addNewTask(
//                newTaskId,
//                "TASK NAME",
//                "DESCRIPTION",
//                30.0,
//            ) {
//                Log.d("TAG_MAIN_MENU", "DONE ADD TASK")
//                ListRepository().editList(
//                        "c5e56812-50ca-42fa-a5c7-07a7a2d6efd7",
//                        newTaskId,
//                        mapOf(
//                            "description" to "Updated task description",
//                        ),
//                    ) {
//                        Log.d("ITEM", "Task edit done")
//                        ListRepository().deleteList(
//                            "c5e56812-50ca-42fa-a5c7-07a7a2d6efd7",
//                            newTaskId,
//                        ) {
//                            Log.d("ITEM", "Task completed")
//                        }
//                    }
//            }
//        }

//        UsersRepository().addUserToHousehold("andrzej@gmail.com", "6d4711ab-b271-4333-800b-941a92b9af5c") {
//            Log.d("ADD_USER", "Sucessfully added $it")
//        }
//        UsersRepository().getFullInfoAboutUsersInHousehold("6d4711ab-b271-4333-800b-941a92b9af5c") {
//            Log.d("ADD_USER", "Sucessfully added $it")
//        }
//        UsersRepository().deleteUserFromHousehold("damian@gmail.com", "6d4711ab-b271-4333-800b-941a92b9af5c") {
//            Log.d("ONSUCCESS", it)
//        }
//        UsersRepository().getFullInfoAboutUsersInHousehold("6d4711ab-b271-4333-800b-941a92b9af5c") {
//            Log.d("ADD_USER", "Sucessfully added $it")
//        }

        menuItems.add(MenuItem("Households", R.drawable.ic_household_icon, R.color.crane_purple_800, "household_list", ""))
        menuItems.add(MenuItem("New household", R.drawable.ic_add_house, R.color.crane_purple_800, "add_household", ""))

        adapter.notifyDataSetChanged()
    }

    override fun onBackPressed() {
    }
}
