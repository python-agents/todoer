package com.pandemiastudio.redo.listactivities

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.pandemiastudio.redo.ListTypes
import com.pandemiastudio.redo.R
import com.pandemiastudio.redo.database.repository.HouseholdRepository
import com.pandemiastudio.redo.database.repository.ListRepository
import nl.bryanderidder.themedtogglebuttongroup.ThemedToggleButtonGroup
import www.sanju.motiontoast.MotionToast
import java.util.*

open class AddListActivity : AppCompatActivity() {
    private var contextG = this

    private lateinit var householdId : String

    private var todoListType = ""
    private var todoListPriority : Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_list)

        householdId = intent.getStringExtra("householdId").toString()

        findViewById<ThemedToggleButtonGroup>(R.id.todoListTypeButtonGroup).setOnSelectListener {
            when(it.id) {
                R.id.TodoListRadioButtonFinancial -> todoListType = ListTypes.FINANCIAL.value
                R.id.TodoListRadioButtonActivity -> todoListType = ListTypes.ACTIVITY.value
            }
        }
        findViewById<ThemedToggleButtonGroup>(R.id.todoListPriorityButtonGroup).setOnSelectListener {
            when(it.id) {
                R.id.TodoListRadioButtonLow -> todoListPriority = 1
                R.id.TodoListRadioButtonMedium -> todoListPriority = 2
                R.id.TodoListRadioButtonHigh -> todoListPriority = 3
            }
        }

    }

    open fun createToDoListOnClick(view: View) {
        val todoListName = findViewById<EditText>(R.id.TodoListName).text.toString()
        val todoListDescription = findViewById<EditText>(R.id.TodoListDescription).text.toString()

        if (todoListName.isNotEmpty() && todoListDescription.isNotEmpty() && todoListPriority > 0 && todoListType.isNotEmpty()) {

            val c = Calendar.getInstance()

            val datePickerListener = DatePickerDialog.OnDateSetListener{ _, year, month, dayOfMonth ->

                ListRepository().addNewTaskList(householdId, todoListName,
                    todoListType, todoListDescription, todoListPriority, Calendar.getInstance().also {
                    it.set(year, month, dayOfMonth)
                }.timeInMillis, 0.0) {
                }
                val intent = Intent(contextG, ListList::class.java)
                intent.putExtra("householdId", householdId)
                startActivity(intent)
            }
            DatePickerDialog(this, datePickerListener, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show()
        }
        else {
            MotionToast.createColorToast(this,
                "Failed ☹️",
                "Please fill all fields",
                MotionToast.TOAST_WARNING,
                MotionToast.GRAVITY_BOTTOM,
                MotionToast.LONG_DURATION,
                ResourcesCompat.getFont(this,R.font.helvetica_regular))
        }
    }
}