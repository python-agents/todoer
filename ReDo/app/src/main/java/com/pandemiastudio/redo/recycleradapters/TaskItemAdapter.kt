package com.pandemiastudio.redo.recycleradapters

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat.startActivity
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.pandemiastudio.redo.AddTask
import com.pandemiastudio.redo.EditTask
import com.pandemiastudio.redo.ListTypes
import com.pandemiastudio.redo.R
import com.pandemiastudio.redo.database.TasksRepository
import com.pandemiastudio.redo.database.repository.ListRepository
import com.pandemiastudio.redo.database.schema.TasksSchema
import www.sanju.motiontoast.MotionToast

class TaskItemAdapter(private var context: Context, private var taskList: ArrayList<TasksSchema>,
    private val listType: String, private val listId : String, private val householdId: String) :
    RecyclerView.Adapter<TaskItemAdapter.ViewHolder>(){

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val titleText : TextView = view.findViewById(R.id.task_item_title)
        val descriptionText : TextView = view.findViewById(R.id.task_item_description)
        val backgroundLayout : LinearLayout = view.findViewById(R.id.task_item_background)
        val descriptionLayout : LinearLayout = view.findViewById(R.id.task_item_description_layout)
        val checkImageView : ImageView = view.findViewById(R.id.task_item_completed_icon)
        val taskCost : TextView = view.findViewById(R.id.task_item_cost)

        val deleteButton : ImageView = view.findViewById(R.id.task_item_delete_button)
        val completeButton : ImageView = view.findViewById(R.id.task_item_completed_button)

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
                R.layout.task_item,
                parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)  {
        val item = taskList[position]

        holder.titleText.text = item.taskName
        holder.descriptionText.text = item.description
        holder.taskCost.text = "Paid: " +item.cost.toString()

        if(item.completed) {
            setTaskCompleted(holder)
        }

        holder.backgroundLayout.setOnClickListener {
            holder.descriptionLayout.visibility = when (holder.descriptionLayout.visibility) {
                LinearLayout.GONE -> LinearLayout.VISIBLE
                else -> LinearLayout.GONE
            }
        }

        holder.backgroundLayout.setOnLongClickListener{
            if (item.completed){
                return@setOnLongClickListener true
            }
            val intent1 = Intent(context, EditTask::class.java)
            intent1.putExtra("listId", listId)
            intent1.putExtra("listType", listType)
            intent1.putExtra("householdId", householdId)
            intent1.putExtra("id", item.id)
            intent1.putExtra("task_name", item.taskName)
            intent1.putExtra("description", item.description)
            context.startActivity(intent1)

            return@setOnLongClickListener true
        }

        holder.completeButton.setOnClickListener {

            if(listType ==  ListTypes.FINANCIAL.value) {
                showDialog(holder, item)
            } else {
                TasksRepository().updateTaskCompletion(listId, item.id, true) {}
            }

        }

        holder.deleteButton.setOnClickListener {

            ListRepository().updateListCost(householdId, listId, -item.cost) {}

            val t_position : Int = taskList.indexOf(item)
            taskList.removeAt(t_position)
            notifyItemRemoved(t_position)
            notifyItemRangeChanged(t_position, taskList.size)
            TasksRepository().deleteTask(listId, item.id) {}
        }

    }


    override fun getItemCount() = taskList.size

    private fun showDialog(holder: ViewHolder, item : TasksSchema) {

        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setTitle("Cost of item")
        val input = EditText(context)
        input.inputType = InputType.TYPE_CLASS_TEXT
        builder.setView(input)
        builder.setPositiveButton("Insert") { dialog, _ ->
            var m_Text = input.text.toString()
            var cost = m_Text.toDoubleOrNull()

            if(cost == null) {
                dialog.cancel()
                MotionToast.createColorToast(
                    context as Activity,
                    "Failed ☹",
                    "Please enter a number",
                    MotionToast.TOAST_WARNING,
                    MotionToast.GRAVITY_BOTTOM,
                    MotionToast.LONG_DURATION,
                    ResourcesCompat.getFont(context,R.font.helvetica_regular))
            } else if(cost <0) {
                dialog.cancel()
                MotionToast.createColorToast(
                    context as Activity,
                    "Failed ☹",
                    "Please enter a positive number",
                    MotionToast.TOAST_WARNING,
                    MotionToast.GRAVITY_BOTTOM,
                    MotionToast.LONG_DURATION,
                    ResourcesCompat.getFont(context,R.font.helvetica_regular))
            }
            else {
                ListRepository().updateListCost(householdId, listId, cost) {}
                TasksRepository().updateTaskCost(listId, item.id, cost) {}
                TasksRepository().updateTaskCompletion(listId, item.id, true) {}
            }
        }
        builder.setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }

        builder.show()
    }

    private fun setTaskCompleted(holder: ViewHolder) {
        holder.completeButton.visibility = ImageView.GONE
        holder.checkImageView.visibility = ImageView.VISIBLE
        holder.backgroundLayout.setBackgroundColor(context.getColor(R.color.completed))
        if(listType ==  ListTypes.FINANCIAL.value) {
            holder.taskCost.visibility = TextView.VISIBLE
        }
    }

}