package com.pandemiastudio.redo.database.repository

import android.util.Log
import com.pandemiastudio.redo.database.CloudDatabase
import com.pandemiastudio.redo.database.schema.ListSchema
import java.util.*

class ListRepository : CloudDatabase() {
    override val TAG = "${super.TAG}_LIST"

    fun addNewTaskList(
        householdId: String,
        listName: String,
        type: String,
        description: String,
        priority: Long,
        dueDate : Long,
        cost: Double = 0.0,
        onSuccess: () -> Unit
    ) {
        val listId = UUID.randomUUID().toString()
        val listRef = database.getReference("households/$householdId/lists/$listId")
        listRef.keepSynced(true)
        listRef
            .setValue(
                ListSchema("", type, listName, description, priority, false, dueDate, cost)
            ).addOnSuccessListener {
                onSuccess()
            }.addOnFailureListener {
                Log.d(TAG, "Error during add heousehold to user $it")
            }
    }

    fun updateListCost(householdId: String, listId: String, cost: Double, callback: () -> Any) {
        var myRef = database.getReference("households/$householdId/lists/$listId")
        myRef.keepSynced(true)
        var oldCost = 0.0

        myRef.child("cost").get().addOnSuccessListener {
            oldCost = if (it.value is Long ) {
                (it.value as Long).toDouble()
            } else {
                it.value as Double
            }


            val newCost = oldCost+cost


            myRef = database.getReference("households/$householdId/lists/$listId/cost")
            myRef.keepSynced(true)
            myRef.setValue(newCost).addOnSuccessListener {
                callback()
            }
        }
    }

    fun updateListCompleted(householdId: String, listId: String, completed: Boolean, callback: () -> Any) {
        var myRef = database.getReference("households/$householdId/lists/$listId")
        myRef.keepSynced(true)
        myRef.child("completed").setValue(completed).addOnSuccessListener {
            callback()
        }
    }

    fun editList(
        householdId: String,
        listId: String,
        fieldsToUpdate: Map<String, Any>,
        onSuccess: () -> Unit
    ) {
        val listRef = database.getReference("households/$householdId/lists/$listId")
        listRef.keepSynced(true)
        listRef
            .updateChildren(
                fieldsToUpdate
            ).addOnSuccessListener {
                onSuccess()
            }.addOnFailureListener {
                Log.d(TAG, "Error during add household to user $it")
            }
    }

    fun deleteList(
        householdId: String,
        listId: String,
        onSuccess: () -> Unit
    ) {
        val listRef = database.getReference("households/$householdId/lists/$listId")
        listRef.keepSynced(true)
        listRef
            .removeValue()
            .addOnSuccessListener {
                database.getReference("lists/$listId")
                    .removeValue()
                    .addOnSuccessListener {
                        onSuccess()
                    }
            }.addOnFailureListener {
                Log.d(TAG, "Error during add heousehold to user $it")
            }
    }
}