package com.pandemiastudio.redo.recycleradapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.pandemiastudio.redo.HouseholdMenu
import com.pandemiastudio.redo.MainActivity
import com.pandemiastudio.redo.R
import com.pandemiastudio.redo.data.MenuItem
import com.pandemiastudio.redo.householdactivities.AddHousehold
import com.pandemiastudio.redo.householdactivities.AddMember
import com.pandemiastudio.redo.householdactivities.HouseholdListActivity
import com.pandemiastudio.redo.householdactivities.HouseholdMembersActivity
import com.pandemiastudio.redo.listactivities.AddListActivity
import com.pandemiastudio.redo.listactivities.ListList
import java.nio.channels.InterruptedByTimeoutException

class MenuItemAdapter(private var context: Context, private var itemList: ArrayList<MenuItem>):
RecyclerView.Adapter<MenuItemAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val buttonImage : ImageView = view.findViewById(R.id.mainMenuItemImageView)
        val titleText : TextView = view.findViewById(R.id.mainMenuItemTextView)
        val backgroundLayout : LinearLayout = view.findViewById(R.id.mainMenuItemBackground)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
                R.layout.menu_item,
                parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = itemList[position]



        holder.buttonImage.setImageResource(item.buttonImage)
        holder.titleText.text = item.buttonText
        holder.backgroundLayout.setBackgroundColor(context.getColor(item.backgroundColor))

        holder.backgroundLayout.setOnClickListener() {
            val intent : Intent = when(item.nextActivity) {
                "household_list" -> Intent(context, HouseholdListActivity::class.java)
                "add_household" -> Intent(context, AddHousehold::class.java)
                "newlistactivity" -> Intent(context, AddListActivity::class.java).putExtra("householdId", item.additionalData)
                "showlist" -> Intent(context, ListList::class.java).putExtra("householdId", item.additionalData)
                "showMembers" -> Intent(context, HouseholdMembersActivity::class.java).putExtra("householdId", item.additionalData)
                "addMember" -> Intent(context, AddMember::class.java).putExtra("householdId", item.additionalData)
                else -> Intent(context, MainActivity::class.java)
            }
            context.startActivity(intent)
        }

    }


    override fun getItemCount() = itemList.size

}