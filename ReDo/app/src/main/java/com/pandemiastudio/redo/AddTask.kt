package com.pandemiastudio.redo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.pandemiastudio.redo.database.TasksRepository
import com.pandemiastudio.redo.database.repository.ListRepository
import com.pandemiastudio.redo.database.schema.UserSchema
import com.pandemiastudio.redo.taskactivities.TaskListActivity
import www.sanju.motiontoast.MotionToast

open class AddTask: AppCompatActivity() {
    open val resultIntent : Intent = Intent()
    // FINANCIAL | ACTIVITY
    open var taskType : String = ""
    private lateinit var listId : String
    private lateinit var listType : String
    private lateinit var householdId : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_task)

        listId = intent.getStringExtra("listId").toString()
        listType = intent.getStringExtra("listType").toString()
        householdId = intent.getStringExtra("householdId").toString()
    }

    open fun submit(view: View) {
        val title = findViewById<EditText>(R.id.fillTitle).text.toString()
        val description = findViewById<EditText>(R.id.fillDesc).text.toString()

        if (title.isNotEmpty() && description.isNotEmpty()){


            val intent = Intent(applicationContext, TaskListActivity::class.java)
            intent.putExtra("listId", listId)
            intent.putExtra("listType", listType)
            intent.putExtra("householdId", householdId)
            listType = intent.getStringExtra("listType").toString()
            val taskID = resultIntent.getStringExtra("taskID")
            Log.d("check", "taskID $taskID")
                if (taskID != null) {
                    Log.d("check", "notEmpty")
                    val updatedArray = mapOf("taskName" to title, "description" to description)
                    TasksRepository().editTask(
                        listId,
                        taskID.toString(),
                        updatedArray
                    ) {
                        ListRepository().updateListCompleted(householdId, listId, false ) {
                            startActivity(intent)
                        }
                    }
                } else {
                    Log.d("check", "noEdit")
                    TasksRepository().addNewTask(
                        listId,
                        title,
                        description,
                        0.0
                    ) {
                    }
                    ListRepository().updateListCompleted(householdId, listId, false ) {}
                    startActivity(intent)
                }
        }

        else {
            MotionToast.createColorToast(this,
                    "Failed ☹️",
                    "Please fill all fields",
                    MotionToast.TOAST_WARNING,
                    MotionToast.GRAVITY_BOTTOM,
                    MotionToast.LONG_DURATION,
                    ResourcesCompat.getFont(this, R.font.helvetica_regular))
        }
    }
}