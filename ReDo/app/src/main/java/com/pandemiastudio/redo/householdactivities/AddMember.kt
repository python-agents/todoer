package com.pandemiastudio.redo.householdactivities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProvider
import com.pandemiastudio.redo.MainMenu
import com.pandemiastudio.redo.R
import com.pandemiastudio.redo.database.repository.UsersRepository
import www.sanju.motiontoast.MotionToast


class AddMember : AppCompatActivity() {
    private var householdId : String = "0"
    private var memberMail : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_member)
        householdId = intent.getStringExtra("householdId").toString()

    }
    /**
     * TODO: "trigger" twórca householdu atumatycznie dodaje się
     *      do household w tabeli user per household
     */
    fun addMemberOnClick(view: View) {
        val editTextMail = findViewById<EditText>(R.id.chooseMemberEditText)
        memberMail = editTextMail.text.toString()
        if (memberMail.isNotEmpty() && householdId != "0") {
            Log.d("test", "Dodawnaie")
            UsersRepository().addUserToHousehold(memberMail, householdId) {
                Log.d("test", it)
                when (it){
                "ALREADY_EXISTS" -> {
                    MotionToast.createColorToast(
                        this,
                        "User already exists",
                        "",
                        MotionToast.TOAST_WARNING,
                        MotionToast.GRAVITY_BOTTOM,
                        MotionToast.LONG_DURATION,
                        ResourcesCompat.getFont(this, R.font.helvetica_regular)
                    )
                }
                "USER_NOT_FOUND" -> {
                    MotionToast.createColorToast(
                        this,
                        "User not found",
                        "",
                        MotionToast.TOAST_WARNING,
                        MotionToast.GRAVITY_BOTTOM,
                        MotionToast.LONG_DURATION,
                        ResourcesCompat.getFont(this, R.font.helvetica_regular)
                    )
                }
                "OK" -> {
                    MotionToast.createColorToast(
                        this,
                        "User added",
                        "",
                        MotionToast.TOAST_SUCCESS,
                        MotionToast.GRAVITY_BOTTOM,
                        MotionToast.LONG_DURATION,
                        ResourcesCompat.getFont(this, R.font.helvetica_regular)
                    )
                    // przekeierowanie do tyłu
                    super.onBackPressed()
                    val intent = Intent(this, HouseholdMembersActivity::class.java).putExtra("householdId", householdId)
                    startActivity(intent)
                }
                "ALREADY_ASSIGNED" -> {
                MotionToast.createColorToast(
                    this,
                    "Already assigned",
                    "",
                    MotionToast.TOAST_WARNING,
                    MotionToast.GRAVITY_BOTTOM,
                    MotionToast.LONG_DURATION,
                    ResourcesCompat.getFont(this, R.font.helvetica_regular)
                )
            }
                else -> {
                    MotionToast.createColorToast(
                        this,
                        "Some error occured",
                        "",
                        MotionToast.TOAST_WARNING,
                        MotionToast.GRAVITY_BOTTOM,
                        MotionToast.LONG_DURATION,
                        ResourcesCompat.getFont(this, R.font.helvetica_regular)
                    )
                }
            }
            }
        }
        else {
            if (!memberMail.isNotEmpty()) {
                MotionToast.createColorToast(
                    this,
                    "Failed ☹️",
                    "Please choose member",
                    MotionToast.TOAST_WARNING,
                    MotionToast.GRAVITY_BOTTOM,
                    MotionToast.LONG_DURATION,
                    ResourcesCompat.getFont(this, R.font.helvetica_regular)
                )
            }
            else if (householdId == "0") {
                MotionToast.createColorToast(
                    this,
                    "Failed ☹️",
                    "Incorrect household selected",
                    MotionToast.TOAST_WARNING,
                    MotionToast.GRAVITY_BOTTOM,
                    MotionToast.LONG_DURATION,
                    ResourcesCompat.getFont(this, R.font.helvetica_regular)
                )
            }
        }
    }
}