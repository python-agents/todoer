package com.pandemiastudio.redo.recycleradapters

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.pandemiastudio.redo.R
import com.pandemiastudio.redo.database.repository.UsersRepository
import com.pandemiastudio.redo.database.schema.UserSchema
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation
import www.sanju.motiontoast.MotionToast


class MemberItemAdapter(private var context: Context, private var memberList: ArrayList<UserSchema>,
                        private var householdId: String):
    RecyclerView.Adapter<MemberItemAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val nicnameText : TextView = view.findViewById(R.id.member_item_name)
        val mailText : TextView = view.findViewById(R.id.member_item_mail)
        val avatarImage : ImageView = view.findViewById(R.id.member_item_avatar)
        val deleteButton : ImageView = view.findViewById(R.id.member_item_delete_button)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.member_item,
            parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = memberList[position]
        holder.nicnameText.text = if(item.nickName == "null") "" else item.nickName
        holder.mailText.text = item.email
        setAvatarImage(holder.avatarImage, item)

        holder.deleteButton.setOnClickListener {
            val t_position: Int = memberList.indexOf(item)
            UsersRepository().deleteUserFromHousehold(item.email, householdId){
                if (it == "CANNOT_REMOVE_ADMIN") {
                    MotionToast.createColorToast(
                        context as Activity,
                        "Cannot remove admin",
                        "",
                        MotionToast.TOAST_WARNING,
                        MotionToast.GRAVITY_BOTTOM,
                        MotionToast.LONG_DURATION,
                        ResourcesCompat.getFont(context, R.font.helvetica_regular)
                    )
                }
                else if (it == "USER_NOT_FOUND"){
                    MotionToast.createColorToast(
                        context as Activity,
                        "User not found",
                        "",
                        MotionToast.TOAST_WARNING,
                        MotionToast.GRAVITY_BOTTOM,
                        MotionToast.LONG_DURATION,
                        ResourcesCompat.getFont(context, R.font.helvetica_regular)
                    )
                }
                else if (it == "OK"){
                    MotionToast.createColorToast(
                        context as Activity,
                        "User deleted",
                        "",
                        MotionToast.TOAST_SUCCESS,
                        MotionToast.GRAVITY_BOTTOM,
                        MotionToast.LONG_DURATION,
                        ResourcesCompat.getFont(context, R.font.helvetica_regular)
                    )
                    memberList.removeAt(t_position)
                    notifyItemRemoved(t_position)
                    notifyItemRangeChanged(t_position, memberList.size)
                }
                else if (it == "NOT_JOINED"){
                    MotionToast.createColorToast(
                        context as Activity,
                        "Not joined",
                        "",
                        MotionToast.TOAST_WARNING,
                        MotionToast.GRAVITY_BOTTOM,
                        MotionToast.LONG_DURATION,
                        ResourcesCompat.getFont(context, R.font.helvetica_regular)
                    )
                }
                else {
                    MotionToast.createColorToast(
                        context as Activity,
                        "Some error occured",
                        "",
                        MotionToast.TOAST_WARNING,
                        MotionToast.GRAVITY_BOTTOM,
                        MotionToast.LONG_DURATION,
                        ResourcesCompat.getFont(context, R.font.helvetica_regular)
                    )
                }
            }
        }

    }

    private fun setAvatarImage(imageView: ImageView, item: UserSchema) {
        val transformation: Transformation = RoundedTransformationBuilder()
            .borderColor(Color.BLACK)
            .borderWidthDp(3f)
            .cornerRadiusDp(30f)
            .oval(false)
            .build()

        if (item.imageUrl != "null") {
            Picasso.get()
                .load(item.imageUrl)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(imageView)
        } else {
            imageView.setImageResource(R.drawable.default_profile_img)
        }
    }

    override fun getItemCount() = memberList.size


}