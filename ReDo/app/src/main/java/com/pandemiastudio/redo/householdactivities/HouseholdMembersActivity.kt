package com.pandemiastudio.redo.householdactivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pandemiastudio.redo.HouseholdMenu
import com.pandemiastudio.redo.MainMenu
import com.pandemiastudio.redo.R
import com.pandemiastudio.redo.database.repository.HouseholdRepository
import com.pandemiastudio.redo.database.repository.UsersRepository
import com.pandemiastudio.redo.database.schema.HouseholdSchema
import com.pandemiastudio.redo.database.schema.UserSchema
import com.pandemiastudio.redo.recycleradapters.HouseholdItemAdapter
import com.pandemiastudio.redo.recycleradapters.MemberItemAdapter

class HouseholdMembersActivity : AppCompatActivity() {

    private var householdsMembers = ArrayList<UserSchema>()
    private lateinit var  adapter : MemberItemAdapter
    private lateinit var householdId : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_household_members)
        householdId = intent.getStringExtra("householdId").toString()

        adapter = MemberItemAdapter(this, householdsMembers, householdId)

        val recyclerView : RecyclerView = findViewById(R.id.householdMembersRecycler)
        readMembersFromDatabase()
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

    }
    private fun readMembersFromDatabase() {

        UsersRepository().getFullInfoAboutUsersInHousehold(householdId) {
            householdsMembers.clear()
            it.forEach {
                householdsMembers.add(it)
                adapter.notifyDataSetChanged()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        val bIntent = Intent(this, HouseholdMenu::class.java).putExtra("householdId", householdId)
        startActivity(bIntent)
    }
}