package com.pandemiastudio.redo.data

data class MenuItem(val buttonText: String,  val buttonImage: Int, val backgroundColor : Int,
                        val nextActivity : String, val additionalData : String) {
}