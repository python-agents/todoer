package com.pandemiastudio.redo

enum class ListTypes(val value: String) {
    FINANCIAL("Financial"),
    ACTIVITY("Activity")
}