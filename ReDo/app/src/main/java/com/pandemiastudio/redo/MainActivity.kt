package com.pandemiastudio.redo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import com.pandemiastudio.redo.auth.LoginActivity
import com.pandemiastudio.redo.database.CloudDatabase

class MainActivity : AppCompatActivity() {
    private val TAG: String? = "CLOUD_DB"

    override fun onStart() {
        super.onStart()
         val user = Firebase.auth.currentUser
         if (user != null) {
             val tmpIntent = Intent(this, MainMenu::class.java)
             startActivity(tmpIntent)
             Log.d("MAIN", "Aaaa: ${user.uid}")
             finish()
         } else {
             val tmpIntent = Intent(this, LoginActivity::class.java)
             startActivity(tmpIntent)
             finish()
             Log.d("MAIN", "Bbbb")
         }
    }
    override fun onBackPressed() {
    }
}
