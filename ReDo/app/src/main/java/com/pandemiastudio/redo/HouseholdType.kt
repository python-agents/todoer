package com.pandemiastudio.redo

enum class HouseholdType(val value: String) {
    FAMILY("Family"),
    DORMITORY("Dormitory"),
    ROOMMATES("Roommates")
}