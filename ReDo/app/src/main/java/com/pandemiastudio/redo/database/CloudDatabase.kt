package com.pandemiastudio.redo.database

import com.google.firebase.database.FirebaseDatabase

open class CloudDatabase() {
    protected open val TAG: String? = "CLOUD_DB"

    companion object Factory {
        var databaseInstance: FirebaseDatabase? = null

        val database: FirebaseDatabase
            get() {
                if (databaseInstance == null) {
                    this.databaseInstance = FirebaseDatabase.getInstance(
                        "https://todoapp-fccdd-default-rtdb.europe-west1.firebasedatabase.app/"
                    )
                    this.databaseInstance!!.setPersistenceEnabled(true)
                }
                return databaseInstance!!
            }
    }
}
