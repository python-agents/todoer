package com.pandemiastudio.redo.householdactivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pandemiastudio.redo.MainMenu
import com.pandemiastudio.redo.R
import com.pandemiastudio.redo.database.repository.HouseholdRepository
import com.pandemiastudio.redo.database.schema.HouseholdSchema
import com.pandemiastudio.redo.recycleradapters.HouseholdItemAdapter

class HouseholdListActivity : AppCompatActivity() {

    private var householdsList = ArrayList<HouseholdSchema>()
    private val adapter = HouseholdItemAdapter(this, householdsList)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_household_list)

        val recyclerView : RecyclerView = findViewById(R.id.householdListRecycler)
        readHouseholdsFromDatabase()
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

    }
    private fun readHouseholdsFromDatabase() {

        HouseholdRepository().getUserHouseholds {
            householdsList.add(it)
            adapter.notifyDataSetChanged()
        }


    }

    override fun onBackPressed() {
        super.onBackPressed()

        val bIntent = Intent(this, MainMenu::class.java)
        startActivity(bIntent)
    }
}