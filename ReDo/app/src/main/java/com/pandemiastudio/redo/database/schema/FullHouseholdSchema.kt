package com.pandemiastudio.redo.database.schema

import java.util.*

data class FullHouseholdSchema(
    var id: String = "",
    var adminId: String = "",
    var users: List<String> = listOf(),
    var lists: ArrayList<ListSchema>
)
