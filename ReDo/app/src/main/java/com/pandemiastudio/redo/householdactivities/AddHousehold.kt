package com.pandemiastudio.redo.householdactivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.core.content.res.ResourcesCompat
import com.pandemiastudio.redo.HouseholdType
import com.pandemiastudio.redo.MainMenu
import com.pandemiastudio.redo.R
import com.pandemiastudio.redo.database.repository.HouseholdRepository
import nl.bryanderidder.themedtogglebuttongroup.ThemedToggleButtonGroup
import www.sanju.motiontoast.MotionToast

class AddHousehold : AppCompatActivity() {

    private var householdType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_household)

        findViewById<ThemedToggleButtonGroup>(R.id.addHouseholdThemedButtonGroup).setOnSelectListener {
            when(it.id) {
                R.id.addHouseholdRadioButtonFamily -> householdType = HouseholdType.FAMILY.value
                R.id.addHouseholdRadioButtonDormitory -> householdType = HouseholdType.DORMITORY.value
                R.id.addHouseholdRadioButtonRoommates -> householdType = HouseholdType.ROOMMATES.value
            }
        }
    }

    fun createHouseholdOnClick(view: View) {
        val householdName = findViewById<EditText>(R.id.editTextTextPersonName).text.toString()


        if (householdName.isNotEmpty() && householdType != "") {

            HouseholdRepository().addNewHousehold(householdName, householdType) {
                Log.d("TAG_ADD_HOUSEHOLD", "DONE")
            }

            val intent = Intent(this, MainMenu::class.java)
            startActivity(intent)
        }
        else {
            if (!householdName.isNotEmpty() && householdType == "") {
                MotionToast.createColorToast(this,
                        "Failed ☹️",
                        "Please fill all fields",
                        MotionToast.TOAST_WARNING,
                        MotionToast.GRAVITY_BOTTOM,
                        MotionToast.LONG_DURATION,
                        ResourcesCompat.getFont(this, R.font.helvetica_regular))
            }
            else if (!householdName.isNotEmpty()) {
                MotionToast.createColorToast(this,
                        "Failed ☹️",
                        "Please fill household name",
                        MotionToast.TOAST_WARNING,
                        MotionToast.GRAVITY_BOTTOM,
                        MotionToast.LONG_DURATION,
                        ResourcesCompat.getFont(this, R.font.helvetica_regular))
            }
            else if (householdType == "") {
                MotionToast.createColorToast(this,
                        "Failed ☹️",
                        "Please select household type",
                        MotionToast.TOAST_WARNING,
                        MotionToast.GRAVITY_BOTTOM,
                        MotionToast.LONG_DURATION,
                        ResourcesCompat.getFont(this, R.font.helvetica_regular))
            }
        }
    }
}