package com.pandemiastudio.redo.database.schema

data class ListSchema (
    var id : String = "",
    var type : String  = "",
    var listName  : String = "",
    var description : String = "",
    var priority : Long,
    var completed : Boolean,
    var dueDate : Long,
    var cost : Double
)