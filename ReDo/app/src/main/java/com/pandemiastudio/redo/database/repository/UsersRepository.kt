package com.pandemiastudio.redo.database.repository

import android.util.Log
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.pandemiastudio.redo.database.CloudDatabase
import com.pandemiastudio.redo.database.schema.UserSchema

class UsersRepository : CloudDatabase() {
    fun getUserInfo(email: String, onSuccess: (UserSchema?) -> Unit) {
        val userInfoRef = database.getReference("userinfo/${email.hashCode()}")
        userInfoRef.keepSynced(true)
        userInfoRef.get()
            .addOnSuccessListener {
                val user = it.getValue(UserSchema::class.java)
                Log.d(TAG, "User $user")
                //userInfoRefSynced(false)
                onSuccess(user)
            }.addOnFailureListener {
                Log.d(TAG, "Error during add heousehold to user $it")
            }
    }

    fun updateUserInfo(onSuccess: () -> Unit) {
        val currentUser = Firebase.auth.currentUser!!
        val userRef = database.getReference("userinfo/${currentUser.email!!.hashCode()}")
        userRef.keepSynced(true)
        userRef
            .setValue(
                UserSchema(
                    currentUser.uid,
                    currentUser.email!!,
                    currentUser.photoUrl.toString(),
                    currentUser.displayName.toString()
                )
            ).addOnSuccessListener {
                onSuccess()
            }.addOnFailureListener {
                Log.d(TAG, "Error during add heousehold to user $it")
            }
    }

    fun addUserToHousehold(userEmail: String, householdId: String, onSuccess: (String) -> Unit) {
        getUserInfo(userEmail) { userToAdd ->
            if (userToAdd == null) {
                onSuccess("USER_NOT_FOUND")
            } else {
                getUserEmailsInHousehold(householdId) { householdEmails ->
                    if(userEmail in householdEmails) {
                        onSuccess("ALREADY_EXISTS")
                    } else {
                        HouseholdRepository().joinUserToExistingHousehold(userToAdd.id, householdId) {
                            if (it == "OK") {
                                val userCount = householdEmails.size
                                val householdUsersRef = database.getReference("households/$householdId/users/$userCount")
                                householdUsersRef.keepSynced(true)
                                householdUsersRef.setValue(userEmail)
                                    .addOnSuccessListener {
                                        onSuccess("OK")
                                    }.addOnFailureListener {
                                        Log.d(TAG, "Error during add heousehold to user $it")
                                    }
                            } else {
                                onSuccess(it)
                            }
                        }
                    }
                }
            }
        }
    }

    fun deleteUserFromHousehold(userEmail: String, householdId: String, onSuccess: (String) -> Unit) {
        getUserInfo(userEmail) { userToDelete ->
            if (userToDelete == null) {
                onSuccess("USER_NOT_FOUND")
            } else {
                val householdRef = database.getReference("households/$householdId/")
                householdRef.keepSynced(true)
                householdRef.get()
                    .addOnSuccessListener { dataSnapshot ->
                        val it = HouseholdRepository().buildHouseholdFromSnapshot(householdId, dataSnapshot)
                        if (it.adminId == userEmail) {
                            onSuccess("CANNOT_REMOVE_ADMIN")
                        } else {
                            getUserEmailsInHousehold(householdId) { householdEmails ->
                                if (userEmail !in householdEmails) {
                                    onSuccess("NOT_JOINED")
                                } else {
                                    val newUserList = householdEmails
                                        .stream()
                                        .filter { it != userEmail }
                                        .toArray()
                                        .toList() as List<String>

                                    HouseholdRepository().updateHouseholdUserList(
                                        householdId,
                                        newUserList
                                    ) {
                                        val userHouseholdsRef = database.getReference("users/${userToDelete.id}/households/$householdId")
                                        userHouseholdsRef.keepSynced(true)
                                        userHouseholdsRef.removeValue()
                                            .addOnSuccessListener {
                                                onSuccess("OK")
                                                Log.d("DELETE_USER", "OK")
                                            }
                                    }
                                }
                            }
                        }
                    }
            }
        }
    }

    fun getFullInfoAboutUsersInHousehold(householdId: String, onSuccess: (ArrayList<UserSchema>) -> Unit) {
        val out = ArrayList<UserSchema>()
        getUserEmailsInHousehold(householdId) {users ->
            users.forEach { userEmail ->
                getUserInfo(userEmail) { user ->
                    out.add(user!!)
                    if (out.size == users.size) {
                        onSuccess(out)
                    }
                }
            }
        }
    }

    private fun getUserEmailsInHousehold(householdId: String, onSuccess: (List<String>) -> Unit) {
        val householdUsersRef = database.getReference("households/$householdId/users")
        householdUsersRef.keepSynced(true)
        householdUsersRef.get()
            .addOnSuccessListener {
                onSuccess(it.value as List<String>)
            }
    }
}