package com.pandemiastudio.redo.database.schema

data class UserSchema(
    val id: String = "",
    val email: String = "",
    val imageUrl: String = "",
    val nickName: String = ""
)