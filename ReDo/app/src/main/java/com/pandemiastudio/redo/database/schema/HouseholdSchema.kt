package com.pandemiastudio.redo.database.schema

data class HouseholdSchema(
    var id: String = "",
    var name: String = "",
    var type: String = "",
    var imageUrl: String = ""
)