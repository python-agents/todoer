package com.pandemiastudio.redo.listactivities

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.textview.MaterialTextView
import com.pandemiastudio.redo.ListTypes
import com.pandemiastudio.redo.R
import com.pandemiastudio.redo.database.repository.HouseholdRepository
import com.pandemiastudio.redo.database.repository.ListRepository
import kotlinx.datetime.*
import kotlinx.datetime.TimeZone
import nl.bryanderidder.themedtogglebuttongroup.ThemedToggleButtonGroup
import www.sanju.motiontoast.MotionToast
import java.util.*

class EditListActivity : AddListActivity() {
    private var contextG = this

    private lateinit var householdId : String
    private lateinit var listId : String

    private var todoListPriority : Long = 0
    private lateinit var dueDate : LocalDateTime

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_list)

        householdId = intent.getStringExtra("householdId").toString()
        listId = intent.getStringExtra("listId").toString()
        Log.d("test", "Household: $householdId, listId: $listId")

        title = intent.getStringExtra("listName") as String
        todoListPriority = intent.getLongExtra("priority", 1)
        dueDate = Instant.fromEpochMilliseconds(intent.getLongExtra("dueDate", 1)).toLocalDateTime(TimeZone.currentSystemDefault())

        Log.d("test", "Priority $todoListPriority")
        val description = intent.getStringExtra("description") as String

        findViewById<EditText>(R.id.TodoListName).append(title)
        findViewById<EditText>(R.id.TodoListDescription).append(description)
        findViewById<ThemedToggleButtonGroup>(R.id.todoListPriorityButtonGroup).selectButton(
            when(todoListPriority.toInt()) {
                1 -> R.id.TodoListRadioButtonLow
                2 -> R.id.TodoListRadioButtonMedium
                3 -> R.id.TodoListRadioButtonHigh
                else -> R.id.TodoListRadioButtonLow
            }
        )
        findViewById<Button>(R.id.addNewTodoListButton).text = "Update list"

        findViewById<MaterialTextView>(R.id.TodoListType).visibility = View.GONE
        findViewById<ThemedToggleButtonGroup>(R.id.todoListTypeButtonGroup).visibility = View.GONE
        findViewById<ThemedToggleButtonGroup>(R.id.todoListPriorityButtonGroup).setOnSelectListener {
            when(it.id) {
                R.id.TodoListRadioButtonLow -> todoListPriority = 1
                R.id.TodoListRadioButtonMedium -> todoListPriority = 2
                R.id.TodoListRadioButtonHigh -> todoListPriority = 3
            }
        }

    }

    override fun createToDoListOnClick(view: View) {
        val todoListName = findViewById<EditText>(R.id.TodoListName).text.toString()
        val todoListDescription = findViewById<EditText>(R.id.TodoListDescription).text.toString()

        if (todoListName.isNotEmpty() && todoListDescription.isNotEmpty() && todoListPriority > 0) {

            val c = Calendar.getInstance()
            c.set(dueDate.year, dueDate.monthNumber - 1, dueDate.dayOfMonth)

            val datePickerListener = DatePickerDialog.OnDateSetListener{ _, year, month, dayOfMonth ->
                val updatedArray = mapOf("listName" to todoListName, "description" to todoListDescription,
                    "priority" to todoListPriority, "dueDate" to Calendar.getInstance().also {
                        it.set(year, month, dayOfMonth)
                    }.timeInMillis)
                ListRepository().editList(householdId, listId, updatedArray) {
                    val intent = Intent(contextG, ListList::class.java)
                    intent.putExtra("householdId", householdId)
                    startActivity(intent)
                }

            }
            DatePickerDialog(this, datePickerListener, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show()
        }
        else {
            MotionToast.createColorToast(this,
                "Failed ☹️",
                "Please fill all fields",
                MotionToast.TOAST_WARNING,
                MotionToast.GRAVITY_BOTTOM,
                MotionToast.LONG_DURATION,
                ResourcesCompat.getFont(this,R.font.helvetica_regular))
        }
    }
}
