package com.pandemiastudio.redo

import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pandemiastudio.redo.data.MenuItem
import com.pandemiastudio.redo.householdactivities.HouseholdListActivity
import com.pandemiastudio.redo.recycleradapters.MenuItemAdapter

class HouseholdMenu : AppCompatActivity() {
    private val menuItems = ArrayList<MenuItem>()
    private val adapter = MenuItemAdapter(this, menuItems)
    private lateinit var householdId : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_household_menu)

        val recyclerView: RecyclerView = findViewById(R.id.mainMenuRecycler)

        householdId = intent.getStringExtra("householdId") as String

        addItems()

        recyclerView.adapter = adapter

        var spanCount = when (resources.configuration.orientation) {
            Configuration.ORIENTATION_PORTRAIT -> 2
            else -> 3
        }

        recyclerView.layoutManager = GridLayoutManager(this, spanCount)
    }

    private fun addItems() {
        menuItems.add(MenuItem("List", R.drawable.ic_list, R.color.crane_purple_800, "showlist", householdId))
        menuItems.add(MenuItem("New list", R.drawable.ic_add_list, R.color.crane_purple_800, "newlistactivity", householdId))
        menuItems.add(MenuItem("Members", R.drawable.ic_user, R.color.crane_purple_800, "showMembers", householdId))
        menuItems.add(MenuItem("Add Member", R.drawable.ic_new_user, R.color.crane_purple_800, "addMember", householdId))
        adapter.notifyDataSetChanged()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        val bIntent = Intent(this, HouseholdListActivity::class.java)
        startActivity(bIntent)
    }
}