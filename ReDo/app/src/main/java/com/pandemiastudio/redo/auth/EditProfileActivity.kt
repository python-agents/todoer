package   com.pandemiastudio.redo.auth

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.scale
import androidx.core.view.drawToBitmap
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.pandemiastudio.redo.MainActivity
import com.pandemiastudio.redo.R
import com.pandemiastudio.redo.database.repository.UsersRepository
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation
import kotlinx.android.synthetic.main.activity_edit_profile.*
import www.sanju.motiontoast.MotionToast
import java.io.ByteArrayOutputStream
import java.io.File


class EditProfileActivity : AppCompatActivity() {
    private val GALLERY_REQUEST_CODE = 100
    private val CAMERA_REQUEST_CODE = 101
    private val FILE_NAME = "tmp_photo"
    private var imageUri: Uri? = null
    private var imageChanged = false
    private lateinit var photoFile: File

    private val transformation: Transformation = RoundedTransformationBuilder()
        .borderColor(Color.BLACK)
        .borderWidthDp(3f)
        .cornerRadiusDp(30f)
        .oval(false)
        .build()

    private var imageUrl: String? = null
    private lateinit var username: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        val currentUser = Firebase.auth.currentUser!!
        if (currentUser.photoUrl == null) {
            Picasso.get()
                .load(R.drawable.default_profile_img)
                .transform(transformation)
                .into(avatarImageButton)
        } else {
            Picasso.get()
                .load(currentUser.photoUrl)
                .into(avatarImageButton)
        }
        usernameEditText.setText(currentUser.displayName)
    }

    fun updateAvatarImage(view: View) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Choose resource:")
            .setCancelable(true)
            .setNegativeButton("GALLERY") { dialog, _ ->
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                startActivityForResult(intent, GALLERY_REQUEST_CODE)
                dialog.dismiss()
            }
            .setPositiveButton("CAMERA") { dialog, _ ->
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                photoFile = getPhotoFile(FILE_NAME)

                val fileProvider = FileProvider.getUriForFile(this, "com.pandemiastudio.redo.fileprovider", photoFile)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider)
                if (takePictureIntent.resolveActivity(this.packageManager) != null) {
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE)
                } else {
                    showErrorToast("Unable to open the camera")
                }
                dialog.dismiss()
            }
            .setNeutralButton("BACK") { dialog, _ ->
                dialog.dismiss()
            }
        builder.create().show()
    }

    private fun getPhotoFile(fileName: String): File {
        // Use `getExternalFilesDir` on Context to access package-specific directories.
        val storageDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(fileName, ".jpg", storageDirectory)
    }

    fun storeUser(view: View) {
        try {
            username = usernameEditText.text.toString()
            Log.d("EDIT_PROFILE", "Edit profile!")
            uploadImageToFirebase()
        } catch (e: Error) {
            showErrorToast("Error occured during upload")
        }
    }

    private fun uploadImageToFirebase() {
        if (imageChanged) {
            val filename = Firebase.auth.currentUser!!.uid
            val ref = FirebaseStorage.getInstance().getReference("/avatars/$filename")

            // Get the data from an ImageView as bytes
            var bitmap = avatarImageButton.drawToBitmap()
            val baos = ByteArrayOutputStream()
            bitmap = bitmap.scale(avatarImageButton.width, avatarImageButton.height, false)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos)
            val data = baos.toByteArray()

            var uploadTask = ref.putBytes(data)
            uploadTask.addOnFailureListener {
                Log.d("STORE IMAGE", "FAILED")
            }.continueWithTask {  task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                ref.downloadUrl
            }.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    imageUrl = task.result.toString()
                    Log.d("STORE IMAGE", "$imageUrl")
                    storeUserInfo()
                } else {
                    Log.d("STORE IMAGE", "FAILED")
                }
            }



//            ref.putFile(imageUri!!)
//                    .addOnSuccessListener {
//                        Log.d("EDIT_PROFILE", "Successfully uploaded image!")
//
//                        ref.downloadUrl.addOnSuccessListener {
//                            imageUrl = it.toString()
//                            storeUserInfo()
//                        }
//                    }
//            imageView.isDrawingCacheEnabled = true
//            imageView.buildDrawingCache()
//            val bitmap = (imageView.drawable as BitmapDrawable).bitmap
//            val baos = ByteArrayOutputStream()
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
//            val data = baos.toByteArray()
//
//            var uploadTask = mountainsRef.putBytes(data)
//            uploadTask.addOnFailureListener {
//                // Handle unsuccessful uploads
//            }.addOnSuccessListener { taskSnapshot ->
//                // taskSnapshot.metadata contains file metadata such as size, content-type, etc.
//                // ...
//            }
        } else {
            storeUserInfo()
        }
    }

    private fun storeUserInfo() {
        val user = Firebase.auth.currentUser
        Log.d("EDIT_PROFILE", "Store user info $username $imageUri!")

        val profileUpdates = if (imageUrl != null)
            userProfileChangeRequest {
                displayName = username
                photoUri = Uri.parse(imageUrl)
            }
        else
            userProfileChangeRequest {
                displayName = username
            }


        user!!.updateProfile(profileUpdates)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                        Log.d("EDIT_PROFILE", "User profile updated.")
                        UsersRepository().updateUserInfo {
                            Toast.makeText(
                                applicationContext,
                                "User updated successfully",
                                Toast.LENGTH_SHORT
                            ).show()
                            finish()
                        }
                    } else {
                        Toast.makeText(
                            applicationContext,
                            "Error occured during upload",
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.d("EDIT_PROFILE", "User profile not updated. Unsuccessful.")
                    }
                }.addOnFailureListener {
                    Toast.makeText(
                        applicationContext,
                        "Error occured during upload",
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("EDIT_PROFILE", "User profile update failed.")
                }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == GALLERY_REQUEST_CODE) {
            imageUri = data?.data

            if (imageUri != null) {
                Picasso.get()
                    .load(imageUri)
                    .transform(transformation)
                    .resize(avatarImageButton.width, avatarImageButton.height)
                    .into(avatarImageButton)
                imageChanged = true
            } else {
                showErrorToast("Couldn't pick the image")
            }
        } else if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                imageUri = Uri.fromFile(File(photoFile.absolutePath))
                if (imageUri != null) {
                    Picasso.get()
                        .load(imageUri)
                        .transform(transformation)
                        .resize(avatarImageButton.width, avatarImageButton.height)
                        .into(avatarImageButton)
                    imageChanged = true
                }
            } else {
                showErrorToast("Couldn't pick the image")
            }

        }
    }

    private fun showErrorToast(message: String) {
        MotionToast.createColorToast(this,
            "Failed ☹️",
            message,
            MotionToast.TOAST_WARNING,
            MotionToast.GRAVITY_BOTTOM,
            MotionToast.LONG_DURATION,
            ResourcesCompat.getFont(this, R.font.helvetica_regular))
    }
}

