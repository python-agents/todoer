package com.pandemiastudio.redo.database.repository

import android.util.Log
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import com.pandemiastudio.redo.database.CloudDatabase
import com.pandemiastudio.redo.database.schema.FullHouseholdSchema
import com.pandemiastudio.redo.database.schema.HouseholdSchema
import com.pandemiastudio.redo.database.schema.ListSchema
import java.util.*
import kotlin.collections.ArrayList

class HouseholdRepository : CloudDatabase() {
    override val TAG = "${super.TAG}_HOUSEHOLD"

    fun getUserHouseholds(callback: (HouseholdSchema) -> Any) {
        val userId = Firebase.auth.currentUser!!.uid
        val userHouseholdRef = database.getReference("users/$userId/households")
        userHouseholdRef.keepSynced(true)
        userHouseholdRef.get()
            .addOnSuccessListener { snapshot ->
                snapshot.children.forEach {
                    if (it.value.toString().contains(Regex("users/.*/households/"))) {
                        val userHouseholdUrlRef = database.getReference(it.value.toString())
                        userHouseholdUrlRef.keepSynced(true)
                        userHouseholdUrlRef.get()
                            .addOnSuccessListener { householdSnapshot ->
                                val household = householdSnapshot.getValue(HouseholdSchema::class.java)
                                household!!.id = householdSnapshot.key!!

                                Log.i(TAG, household.toString())
                                callback(household)
                            }
                    } else {
                        val household = it.getValue(HouseholdSchema::class.java)
                        household!!.id = it.key!!

                        Log.i(TAG, household.toString())
                        callback(household)
                    }
                }
            }
    }

    fun buildHouseholdFromSnapshot(householdId: String, dataSnapshot: DataSnapshot): FullHouseholdSchema {
        val map = dataSnapshot.value as Map<String, Any>
        val adminId = map["adminId"] as String
        val users = map["users"] as List<String>
        val listsMap = if(map["lists"] == null) {
            mapOf()
        } else {
            map["lists"] as Map<String, Map<String, Any>>
        }
        val lists : ArrayList<ListSchema> = arrayListOf()

        listsMap.forEach {
            if (listsMap[it.key] != null) {
                val map : Map<String, Any> = listsMap[it.key]!!

                val listSchema = ListSchema(
                    it.key,
                    map["type"]!! as String,
                    map["listName"]!! as String,
                    map["description"]!! as String,
                    map["priority"]!! as Long,
                    map["completed"]!! as Boolean,
                    map["dueDate"]!! as Long,
                    if (map["cost"]!! is Long ) {
                        (map["cost"] as Long).toDouble()
                    } else {
                        map["cost"] as Double
                    }
                )
                lists.add(listSchema)
            }
        }

        return FullHouseholdSchema(householdId, adminId, users, lists)
    }

    fun getHousehold(householdId: String, callback: (FullHouseholdSchema) -> Any) {
        val myRef = database.getReference("households/$householdId/")
        myRef.keepSynced(true)
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if(dataSnapshot.value == null)
                    return

                val out = buildHouseholdFromSnapshot(householdId, dataSnapshot)
                Log.i(TAG, out.toString())
                callback(out)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to load household.", error.toException())
            }
        })
    }

    fun addNewHousehold(
        name: String,
        type: String,
        onSuccess: () -> Unit
    ) {
        val adminId = Firebase.auth.currentUser!!.uid
        val adminEmail = Firebase.auth.currentUser!!.email!!
        val householdId = UUID.randomUUID().toString()

        var isOneReady = false
        val userHouseholdRef = database.getReference("users/$adminId/households/$householdId")
        userHouseholdRef.keepSynced(true)
        userHouseholdRef
            .setValue(
                HouseholdSchema(
                    householdId,
                    name,
                    type,
                    ""
                )
            ).addOnSuccessListener {
                if (isOneReady) {
                    onSuccess()
                } else {
                    isOneReady = true
                }
            }.addOnFailureListener {
                Log.d(TAG, "Error during add heousehold to user $it")
            }

        val householdRef = database.getReference("households/$householdId")
        householdRef.keepSynced(true)
        householdRef.setValue(
                FullHouseholdSchema(
                    householdId,
                    adminEmail,
                    listOf(adminEmail),
                    arrayListOf()
                )
            ).addOnSuccessListener {
                if (isOneReady) {
                    onSuccess()
                } else {
                    isOneReady = true
                }
            }.addOnFailureListener {
                Log.d(TAG, "Error during add heousehold to user $it")
            }
    }

    fun joinUserToExistingHousehold(userId: String,householdId: String, onSuccess: (String) -> Unit) {
        val householdRef = database.getReference("households/$householdId/")
        householdRef.keepSynced(true)
        householdRef.get()
            .addOnSuccessListener { dataSnapshot ->
                val it = HouseholdRepository().buildHouseholdFromSnapshot(householdId, dataSnapshot)
                UsersRepository().getUserInfo(it.adminId) {admin ->
                    val adminId = admin!!.id
                    val userHouseholdRef = database.getReference("users/$userId/households/$householdId")
                    userHouseholdRef.keepSynced(true)
                    userHouseholdRef.get()
                        .addOnSuccessListener { userHousehold ->
                            Log.d("ASSIGNING_USER", userHousehold.toString())
                            if (userHousehold.value != null) {
                                onSuccess("ALREADY_ASSIGNED")
                            } else {
                                val userHouseholdSetterRef = database.getReference("users/$userId/households/$householdId")
                                userHouseholdSetterRef.keepSynced(true)
                                userHouseholdSetterRef.setValue("users/${adminId}/households/$householdId")
                                    .addOnSuccessListener {
                                        onSuccess("OK")
                                    }.addOnFailureListener {
                                        Log.d(TAG, "Error during add heousehold to user $it")
                                    }
                            }
                        }.addOnFailureListener {
                            Log.d(TAG, "Error during add heousehold to user $it")
                        }
            }
        }
    }

    fun updateHouseholdUserList(householdId: String, updatedUserList: List<String>, onSuccess: () -> Unit) {
        val householdRef = database.getReference("households/$householdId/")
        householdRef.keepSynced(true)
        householdRef.get()
            .addOnSuccessListener { dataSnapshot ->
                val it = HouseholdRepository().buildHouseholdFromSnapshot(householdId, dataSnapshot)
                UsersRepository().getUserInfo(it.adminId) {admin ->
                    val usersRef = database.getReference("households/$householdId/users")
                    usersRef.keepSynced(true)
                    usersRef
                        .setValue(updatedUserList)
                        .addOnSuccessListener {
                            onSuccess()
                        }.addOnFailureListener {
                            Log.d(TAG, "Error during add heousehold to user $it")
                        }
                    }
            }
    }
}