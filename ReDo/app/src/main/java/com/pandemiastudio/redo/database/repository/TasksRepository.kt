package com.pandemiastudio.redo.database

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.pandemiastudio.redo.database.schema.ListSchema
import com.pandemiastudio.redo.database.schema.TasksSchema
import java.util.*
import kotlin.collections.ArrayList

class TasksRepository : CloudDatabase() {
    override val TAG = "${super.TAG}_TASKS"

    fun addOnGetTasksListener(listId: String, list : ArrayList<TasksSchema>, callback: (TasksSchema) -> Any) {
        val myRef = database.getReference("lists/$listId/tasks")
        myRef.keepSynced(true)

        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                list.clear()
                dataSnapshot.children.forEach {
                    val tasks = it.getValue(TasksSchema::class.java)!!
                    tasks.id = it.key!!

                    Log.i(TAG, tasks.toString())
                    callback(tasks)
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to load tasks.", error.toException())
            }
        })
    }

    fun editTask(listId: String, taskId: String, updatedFields: Map<String, Any>, callback: () -> Any) {
        val myRef = database.getReference("lists/$listId/tasks/$taskId")
        myRef.keepSynced(true)
        myRef.updateChildren(updatedFields).addOnSuccessListener {
            callback()
        }
    }

    fun updateTaskCompletion(listId: String, taskId: String, completed: Boolean, callback: () -> Any) {
        val myRef = database.getReference("lists/$listId/tasks/$taskId/completed")
        myRef.keepSynced(true)
        myRef.setValue(completed).addOnSuccessListener {
            callback()
        }
    }

    fun updateTaskCost(listId: String, taskId: String, cost: Double, callback: () -> Any) {
        val myRef = database.getReference("lists/$listId/tasks/$taskId/cost")
        myRef.keepSynced(true)
        myRef.setValue(cost).addOnSuccessListener {
            callback()
        }
    }

    fun deleteTask(listId: String, taskId: String, callback: () -> Any) {
        val myRef = database.getReference("lists/$listId/tasks/$taskId")
        myRef.keepSynced(true)
        myRef.removeValue()
            .addOnSuccessListener {
                val myRef = database.getReference("tasks/$taskId")

                callback()
            }
    }

    fun addNewTask(
        listId: String,
        taskName: String,
        description: String,
        cost: Double = 0.0,
        onSuccess: (String) -> Unit
    ) {
        val newTaskId = UUID.randomUUID().toString()
        val tasksRef = database.getReference("lists/$listId/tasks/$newTaskId")
        tasksRef.keepSynced(true)
        tasksRef.setValue(
                TasksSchema(newTaskId, taskName, description, false, cost)
            ).addOnSuccessListener {
                Log.d(TAG, "SUCCESS TASK ADDED $it taskID: $newTaskId list: $listId")
                onSuccess(newTaskId)
            }.addOnFailureListener {
                Log.d(TAG, "Error during add household to user $it")
            }
    }
}